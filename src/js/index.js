$(function(){
    var btn = null;
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e){
        console.log('The Modal Is Showing');
        btn = e.relatedTarget;
        $(btn).removeClass('btn-outline-warning');
        $(btn).addClass('btn-danger');
        $(btn).prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e){
        console.log('The Modal Has Been Shown');
    });

    $('#contacto').on('hide.bs.modal', function (e){
        console.log('The Modal Is Hiding');
    });

    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('The Modal Has Been Hidden');
        $(btn).removeClass('btn-danger');
        $(btn).addClass('btn-outline-warning');
        $(btn).prop('disabled', false);
        btn = null;
    });
});